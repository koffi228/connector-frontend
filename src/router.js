import Vue from 'vue'
import Router from 'vue-router'
import AuthLayout from '@/layouts/Auth'
import AppLayout from '@/layouts/App'
import store from '@/store'

Vue.use(Router)

const router = new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: 'dashboard/analytics',
      component: AppLayout,
      meta: { authRequired: true, hidden: true },
      children: [
        // Dashboards
        {
          path: '/dashboard/analytics',
          meta: {
            title: 'Dashboard Analytics',
          },
          component: () => import('./views/dashboard/analytics'),
        },
        {
          path: '/dataload/scormpackage',
          meta: {
            title: 'Upload Scorm Package',
          },
          component: () => import('./views/dataload/scormpackage'),
        },
        // UI Kits
        {
          path: '/ui-kits/bootstrap',
          meta: {
            title: 'Bootstrap',
          },
          component: () => import('./views/ui-kits/bootstrap'),
        },
        {
          path: '/ui-kits/antd',
          meta: {
            title: 'Ant Design',
          },
          component: () => import('./views/ui-kits/antd'),
        },
        // 404
        {
          path: '/404',
          meta: {
            title: 'Error 404',
          },
          component: () => import('./views/system/404'),
        },
      ],
    },

    // System Pages
    {
      path: '/system',
      component: AuthLayout,
      redirect: '/system/login',
      children: [
        {
          path: '/system/login',
          meta: {
            title: 'Login',
          },
          component: () => import('./views/system/login'),
        },
        {
          path: '/system/forgot-password',
          meta: {
            title: 'Forgot Password',
          },
          component: () => import('./views/system/forgot-password'),
        },
        {
          path: '/system/register',
          meta: {
            title: 'Register',
          },
          component: () => import('./views/system/register'),
        },
        {
          path: '/system/lockscreen',
          meta: {
            title: 'Lockscreen',
          },
          component: () => import('./views/system/lockscreen'),
        },
        {
          path: '/system/404',
          meta: {
            title: 'Error 404',
          },
          component: () => import('./views/system/404'),
        },
        {
          path: '/system/500',
          meta: {
            title: 'Error 500',
          },
          component: () => import('./views/system/500'),
        },
      ],
    },

    // Redirect to 404
    {
      path: '*', redirect: 'system/404', hidden: true,
    },
  ],
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.authRequired)) {
    if (!store.state.user.user) {
      next({
        path: '/system/login',
        query: { redirect: to.fullPath },
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
