import store from '@/store'
import router from '@/router'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/storage'

const config = {
  apiKey: 'AIzaSyCn24FPbIVgLAf6kdzahbP0kxKJCskVaNk',
  authDomain: 'connector-e6c94.firebaseapp.com',
  databaseURL: 'https://connector-e6c94.firebaseio.com',
  projectId: 'connector-e6c94',
  storageBucket: 'connector-e6c94.appspot.com',
  messagingSenderId: '353605865817',
  appId: '1:353605865817:web:e5afeea3e619416217fc48',
}

export default {
  install: (Vue, options) => {
    const firebaseApp = firebase.initializeApp(config)
    const auth = firebaseApp.auth()
    Vue.prototype.$auth = {
      login: async (username, pass) => {
        return auth.signInWithEmailAndPassword(username, pass)
      },
      logout: async () => {
        router.push('/system/login')
        await auth.signOut()
      },
    }
    auth.onAuthStateChanged(user => {
      store.commit('UPDATE_USER', { user })
    })
  },
}
