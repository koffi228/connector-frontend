export const getMenuData = [
  {
    category: true,
    title: 'Dashboards',
  },
  {
    title: 'Home',
    key: 'dashboards',
    icon: 'fe fe-home',
    count: 6,
    children: [
      {
        title: 'Main',
        key: 'dashboardAnalytics',
        url: '/dashboard/analytics',
      },
      {
        title: 'Upload Course',
        key: 'uplaod',
        url: '/dataload/scormpackage',
      },
      // {
      //   title: 'RECENT',
      //   key: 'recent',
      //   url: '/dashboard/analytics',
      // },
      // {
      //   title: 'INTEGRATION',
      //   key: 'integration',
      //   url: '/dashboard/analytics',
      // },
      // {
      //   title: 'LEARNING ACTIVITIES',
      //   key: 'learningactivities',
      //   url: '/dashboard/analytics',
      // },
      // {
      //   title: 'ENTITIES',
      //   key: 'entities',
      //   url: '/dashboard/analytics',
      // },
      // {
      //   title: 'DEPLOYMENT',
      //   key: 'deployment',
      //   url: '/dashboard/analytics',
      // },
      // {
      //   title: 'SUPPORT',
      //   key: 'support',
      //   url: '/dashboard/analytics',
      // },
    ],
  },
  {
    title: 'Recent',
    key: 'recent',
    icon: 'fe fe-home',
    count: 6,
    children: [
      {
        title: 'Entities',
        key: 'entities',
        url: '/dashboard/analytics',
      },
      {
        title: 'PA',
        key: 'pa',
        url: '/dashboard/analytics',
      },
    ],
  },
  {
    title: 'Administrators',
    key: 'administrators',
    icon: 'fe fe-home',
    count: 6,
    children: [
      {
        title: 'Search for administrators',
        key: 'searchforadministrators',
        url: '/dashboard/analytics',
      },
      {
        title: 'Add an administrator',
        key: 'addanadministrator',
        url: '/dashboard/analytics',
      },
      {
        title: 'Manage profiles',
        key: 'manageprofiles',
        url: '/dashboard/analytics',
      },
      {
        title: 'Define administrator form',
        key: 'defineadministratorform',
        url: '/dashboard/analytics',
      },
    ],
  },
  {
    title: 'Integration',
    key: 'integration',
    icon: 'fe fe-home',
    count: 6,
    children: [
      {
        title: 'Configure web services',
        key: 'configurewebservices',
        url: '/dashboard/analytics',
      },
      {
        title: 'Configure plug-ins',
        key: 'configureplugins',
        url: '/dashboard/analytics',
      },
      {
        title: 'Manage authentication options',
        key: 'manageauthenticationoptions',
        url: '/dashboard/analytics',
      },
      {
        title: 'Manage connectors and SSO',
        key: 'manageconnectorsandSSO',
        url: '/dashboard/analytics',
      },
    ],
  },
  {
    title: 'learning activities',
    key: 'learningactivities',
    icon: 'fe fe-home',
    count: 6,
    children: [
      {
        title: 'Manage publishers',
        key: 'configurewebservices',
        url: '/dashboard/analytics',
      },
      {
        title: 'Manage FAQs',
        key: 'managefaqs',
        url: '/dashboard/analytics',
      },
      {
        title: 'Manage glossaries',
        key: 'manageglossaries',
        url: '/dashboard/analytics',
      },
      {
        title: 'Manage locations',
        key: 'managelocations',
        url: '/dashboard/analytics',
      },
      {
        title: 'Manage rooms',
        key: 'managerooms',
        url: '/dashboard/analytics',
      },
      {
        title: 'Gamification',
        key: 'gamification',
        url: '/dashboard/analytics',
      },
      {
        title: 'Manage SCORM test environments',
        key: 'managescormtestenvironments',
        url: '/dashboard/analytics',
      },
    ],
  },
  {
    title: 'Entities',
    key: 'entities',
    icon: 'fe fe-home',
    count: 6,
    children: [
      {
        title: 'Manage entities',
        key: 'manageentities',
        url: '/dashboard/analytics',
      },
      {
        title: 'Customize learner\'s form',
        key: 'customizelearnersform',
        url: '/dashboard/analytics',
      },
    ],
  },
  {
    title: 'Deployment',
    key: 'deployment',
    icon: 'fe fe-home',
    count: 6,
    children: [
      {
        title: 'Functional &amp; display options',
        key: 'functionaldisplayoptions',
        url: '/dashboard/analytics',
      },
      {
        title: 'Security headers\' options',
        key: 'securityheadersoptions',
        url: '/dashboard/analytics',
      },
      {
        title: 'Manage scheduled tasks',
        key: 'Managescheduledtasks',
        url: '/dashboard/analytics',
      },
      {
        title: 'Translations',
        key: 'translations',
        url: '/dashboard/analytics',
      },
      {
        title: 'E-mailing',
        key: 'emailing',
        url: '/dashboard/analytics',
      },
      {
        title: 'Manage styles',
        key: 'managestyles',
        url: '/dashboard/analytics',
      },
    ],
  },
  {
    title: 'SUPPORT',
    key: 'support',
    icon: 'fe fe-home',
    count: 6,
    children: [
      {
        title: 'Manage support activities',
        key: 'managesupportactivities',
        url: '/dashboard/analytics',
      },
      {
        title: 'Access parameters &amp; maintenance',
        key: 'accessparametersmaintenance',
        url: '/dashboard/analytics',
      },
      {
        title: 'Manage help text',
        key: 'managehelptext',
        url: '/dashboard/analytics',
      },
      {
        title: 'About ...',
        key: 'about',
        url: '/dashboard/analytics',
      },
    ],
  },
  {
    category: true,
    title: 'Reports',
  },
  {
    title: 'User Guide',
    key: 'bootstrap',
    icon: 'fe fe-bookmark',
    url: '/ui-kits/bootstrap',
  },
  {
    title: 'Think Tank',
    key: 'antDesign',
    icon: 'fe fe-bookmark',
    url: '/ui-kits/antd',
  },
]
